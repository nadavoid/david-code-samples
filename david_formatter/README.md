# About David Formatter

This is a link field formatter that appends a special query string to external links.

The dynamic components of the query string come from:

- A term ("Market Source") related to the same entity the link is on
- Today's date
- A field ("Issue") on the displayed page
- Title of the displayed page
