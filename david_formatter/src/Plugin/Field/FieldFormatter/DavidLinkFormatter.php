<?php

namespace Drupal\david_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkSeparateFormatter;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

/**
 * Plugin implementation of the 'david_link' formatter.
 *
 * Adds query string to URLs of external links.
 *
 * The target format: ?qs=wb_abc_20180531_IssueOne-firstpage
 *
 * @FieldFormatter(
 *   id = "david_link",
 *   label = @Translation("Separate link text and URL with dynamic query string"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class DavidLinkFormatter extends LinkSeparateFormatter {

  /**
   * Host entity that this field is on.
   */
  private $entity;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $this->entity = $items->getEntity();
    $element = parent::viewElements($items, $langcode);

    // Rebuild the destination URL.
    foreach ($items as $delta => $item) {
      // Only work on external links.
      if (!$item->isExternal()) {
        continue;
      }
      // Add arguments to the item.
      $current_uri = $item->getUrl()->getUri();
      $item->uri = $this->appendDynamicQueryString($current_uri);
      $url = $this->buildUrl($item);
      $element[$delta]['#url'] = $url;

      // Not shown: also set cache by url and date.
    }

    return $element;
  }

  /**
   * Append query string to a URL.
   *
   * @param string $uri
   *   URI that will get a string appended.
   *
   * @return string
   */
  public function appendDynamicQueryString($uri) {
    $fragment = parse_url($uri, PHP_URL_FRAGMENT);
    if (strlen($fragment) > 0) {
      $fragment = '#' . $fragment;
      $uri = str_replace($fragment, '', $uri);
    }
    $prefix = strpos($uri, '?') > 0 ? '&' : '?';
    $qs = http_build_query($this->generateDynamicQueryString());
    return $uri . $prefix . $qs . $fragment;
  }

  /**
   * Generate dynamic query string.
   *
   * @return array
   */
  public function generateDynamicQueryString() {
    $parts[] = $this->segmentFirst();
    $parts[] = $this->segmentMSPlacement();
    $parts[] = $this->segmentTodaysDate();

    // Part four is a related Issue if available + trimmed page title.
    $issue_title = $this->segmentIssue();
    $page_title = $this->segmentPageTitle();
    $parts[] = $issue_title . $page_title;

    // Combine the parts to make the qs.
    $array['qs'] = implode('_', $parts);

    return $array;
  }

  /**
   * Get the first segment, which is a static value.
   *
   * @return string
   */
  private function segmentFirst() {
    return 'wb';
  }

  /**
   * Get the code from related Market Source Placement term.
   *
   * @return string
   */
  private function segmentMSPlacement() {
    $ms_placement_code = '-';
    if ($this->entity->field_ms_placement) {
      $field_ms_placement_id = $this->entity->field_ms_placement->target_id;
    }
    if (!empty($field_ms_placement_id)) {
      $ms_placement_code = Term::load($field_ms_placement_id)->field_ms_code->value;
    }

    return strtolower($ms_placement_code);
  }

  /**
   * Current date in YYYYMMDD format.
   *
   * @return false|string
   */
  private function segmentTodaysDate() {
    return date('Ymd');
  }

  /**
   * Issue related to the active page.
   *
   * @return string
   */
  private function segmentIssue() {
    // Issue comes from the page being displayed, not from the entity that
    // hosts this field, so we check the entity of the current page.
    $route_match = \Drupal::routeMatch();
    $node = $route_match->getParameter('node');
    if (!$node) {
      return '';
    }
    if (!$node->field_page_issues) {
      return '';
    }
    $issue_id = $node->field_page_issues->target_id;
    if (!$issue_id) {
      return '';
    }
    $title = Node::load($issue_id)->getTitle();
    $transliterated = \Drupal::transliteration()->transliterate($title);
    $trimmed = str_replace(' ', '', trim($transliterated));
    $title_prepared = $trimmed . '-';

    return $title_prepared;
  }

  /**
   * Trimmed title of current page.
   *
   * @return string
   */
  private function segmentPageTitle() {
    $route_match = \Drupal::routeMatch();
    $request = \Drupal::request();
    $title = \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
    if (!$title || is_array($title)) {
      return '';
    }
    $transliterated = \Drupal::transliteration()->transliterate($title);
    $title_prepared = str_replace(' ', '', trim($transliterated));

    return $title_prepared;
  }

}
