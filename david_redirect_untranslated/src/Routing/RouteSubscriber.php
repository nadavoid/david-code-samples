<?php

namespace Drupal\david_redirect_untranslated\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\david_redirect_untranslated\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Replace the default node controller with our custom one to redirect if
    // the current node is not translated into the current language.
    if ($route = $collection->get('entity.node.canonical')) {
      // $route->defaults->_controller starts off as
      // '\Drupal\node\Controller\NodeViewController::view'.
      $route->addDefaults([
        '_controller' => '\Drupal\david_redirect_untranslated\Controller\DavidNodeViewController::view',
      ]);
    }
  }
}
