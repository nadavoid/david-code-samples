<?php
namespace Drupal\david_redirect_untranslated\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\node\Controller\NodeViewController;
use Zend\Diactoros\Response\RedirectResponse;

/**
 * Class DavidNodeViewController.
 *
 * Redirect untranslated nodes to original language version.
 *
 * @package Drupal\david_redirect_untranslated\Controller
 */
class DavidNodeViewController extends NodeViewController {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $node, $view_mode = 'full', $langcode = NULL) {
    // If view mode is not 'full', return the default view.
    if ($view_mode != 'full') {
      return parent::view($node, $view_mode, $langcode);
    }

    // If the node exists in the current language, return the default view.
    $current_language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $node_has_current_language = $node->hasTranslation($current_language);
    if ($node_has_current_language) {
      return parent::view($node, $view_mode, $langcode);
    }

    // The node is not translated into the current language, so redirect to its
    // original language.
    $node_language = $node->language();
    $url = $node->toUrl('canonical', ['language' => $node_language])->toString();
    return new RedirectResponse($url);
  }
}
