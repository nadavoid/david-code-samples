# About David Redirect Untranslated

This module redirects to the original language of a node if it is displayed in a
language into which it is not translated. This can be desirable if the UI should
always reflect the language of the node, or if there are accidental links to
translations of untranslated nodes.

An example scenario:

- Site primary language is English.
- Site has French language enabled, using "/fr/" prefix for detection.
- Node 3 has been created, but not translated into any other languages.
- User visits `/fr/node/3`, and is redirected to `/node/3`.
